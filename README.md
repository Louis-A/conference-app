# Devfest App

This app was made as part of a course of mobile programming at IMT Atlantique. It was done using Ionic 2 and Apache Cordova.
Basic features were implemented :
* Homepage with general information
* List of sessions (with link to speakers)
* Detail of a session with ability to take notes, pictures and save everything locally
* List of speakers (with link to sessions)
* Page with phone informations

We also added a serviceworker which caches static assets, and a localstorage cache for dynamic data.

Few features weren't implemented due to the short time frame:
 * Adding a speaker as a contact
 * Error management
 * Better testing (e2e, unit)

## Installation

Install and setup Apache Cordova for your platform as stated [here](https://cordova.apache.org/docs/en/latest/guide/support/index.html).
Then install dependencies with NPM :
```
npm install
```

You can either start a webserver (You won't have acces to native features) with :
```
npm start
```
Or run the application with cordova, depending on your platform.

# Known issue
Of you are on a slow network or device, it can take some time until data is accessible which causes errors, wait a few seconds if it's the case.