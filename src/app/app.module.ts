import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DataProvider } from '../providers/data/data';
import { ComponentsModule } from '../components/components.module';
import { ConferencePageModule } from '../pages/conference/conference.module';
import { SessionsPageModule } from '../pages/sessions/sessions.module';
import { PresentateursPageModule } from '../pages/presentateurs/presentateurs.module';
import { PresentateurPageModule } from '../pages/presentateur/presentateur.module';
import { SessionPageModule } from '../pages/session/session.module';
import { PhonePageModule } from '../pages/phone/phone.module';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
    HttpClientModule,
    ComponentsModule,
    ConferencePageModule,
    SessionsPageModule,
    PresentateursPageModule,
    PresentateurPageModule,
    SessionPageModule,
    PhonePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    Device,
    Network,
    Camera,
    File,
    ImagePicker,
  ]
})
export class AppModule {}
