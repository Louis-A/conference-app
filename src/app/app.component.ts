import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ConferencePage } from '../pages/conference/conference';
import { SessionsPage } from '../pages/sessions/sessions';
import { PresentateursPage } from '../pages/presentateurs/presentateurs';
import { DataProvider } from '../providers/data/data';
import { PhonePage } from '../pages/phone/phone';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = ConferencePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public dataProvider: DataProvider) {
    this.initializeApp();
    dataProvider.getSessions().then(() => {
      dataProvider.getSpeakers().then(() => {
        dataProvider.getTimeSlots().then(() => {
          console.log("Data loaded.");
        });
      });
    });
   
    

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Conference', component: ConferencePage },
      { title: 'Sessions', component: SessionsPage },
      { title: 'Presentateurs', component: PresentateursPage },
      { title: 'Téléphone', component: PhonePage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
