import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Speaker } from '../../model/Speaker';
import { Session } from '../../model/Session';
import { TimeSlot } from '../../model/TimeSlot';
import { Storage } from '@ionic/storage';

/*
  Data provider.

  Manages data retrievement from the devfest api and uses a cache in localstorage.
*/
@Injectable()
export class DataProvider {



  constructor(private http: HttpClient, private storage: Storage) {
    console.log('DataProvider Provider Initialize');
  }

  /**
   * Get the list of speakers.
   */
  public getSpeakers (): Promise<Speaker[]> {
    return this.storage.get('speakers')
      .then((speakers) => {
        if(speakers != null) {
          return speakers;
        } else {
          return this.http.get('https://devfest-nantes-2018-api.cleverapps.io/speakers').toPromise().then(speakers => {
            return this.storage.set('speakers', Object.keys(speakers).map(speakerId => {
              let speaker = speakers[speakerId];
              speaker = new Speaker().deserialize(speaker);
              this.storage.set('speaker_' + speaker.id, speaker);
              return speaker;
            })).then(() => {
              return this.storage.get('speakers').then((speakers) => {
                return speakers;
              });
            });
          });
        }
      });
  }

  /**
   * Returns a Speaker given its id. Null if not found.
   * @param id 
   */
  public getSpeaker(id: number): Promise<Speaker> {
    return this.storage.get("speaker_" + id);
  }

  /**
   * Get all sessions and store them in the localstorage.
   */
  public getSessions() {
    return this.storage.get('sessions')
      .then((sessions) => {
        if(sessions != null) {
          return sessions;
        } else {
          return this.http.get('https://devfest-nantes-2018-api.cleverapps.io/sessions').toPromise().then(sessions => {
            return this.storage.set('sessions', Object.keys(sessions).map(sessionId => {
              let session = sessions[sessionId];
              session = new Session().deserialize(session);
              this.storage.set('session_' + session.id, session);
              return session;
            })).then(() => {
              return this.storage.get('sessions').then((sessions) => {
                return sessions;
              });
            });
          });
        }
      });
  }

  /**
   * Returns a Session given its id. Null if not found.
   * @param id 
   */
  public getSession(id: number): Promise<Session> {
    return this.storage.get('session_' + id);
  }

  /**
   * Get all timeslots.
   */
  public getTimeSlots(): Promise<TimeSlot[]> {
    return this.storage.get('timeslots').then((timeslots) => {
      if(timeslots != null) {
        return timeslots;
      } else {
        return this.http.get("https://devfest-nantes-2018-api.cleverapps.io/schedule").toPromise().then((timeslotsData: any[]) => {
          // Get and format timeslots.  
          let timeslots: TimeSlot[] = [];
          Promise.all(timeslotsData.map((day) => {
            return Promise.all(day.timeslots.map((timeslotData) => {
              let timeslot = new TimeSlot();
              timeslot.date = day.date;
              timeslot.dateReadable = day.dateReadable;
              timeslot.startTime = timeslotData.startTime;
              timeslot.endTime = timeslotData.endTime;
              
              timeslot.sessions = [];
              
              // For each session of this timeslot, we retrieve it from cache.
              return Promise.all(timeslotData.sessions.map((sessionId) => {
                return this.getSession(sessionId).then((session) => {
                  return session;
                });
              })).then((sessions: any) => {
                timeslot.sessions = sessions;
                timeslots.push(timeslot);
              });
            })).then(() => {});
          })).then((promises) => {
            return this.storage.set('timeslots', timeslots).then(() => {
              return this.storage.get('timeslots').then((timeslots) => {
                return timeslots;
              });
            });
          });
        });
      }
    });
  }


}
