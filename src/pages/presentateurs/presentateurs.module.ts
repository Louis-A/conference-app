import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PresentateursPage } from './presentateurs';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PresentateursPage
  ],
  imports: [
    IonicPageModule.forChild(PresentateursPage),
    ComponentsModule
  ],
})
export class PresentateursPageModule {}
