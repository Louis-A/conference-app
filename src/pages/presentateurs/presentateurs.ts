import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Speaker } from '../../model/Speaker';

/**
 * Generated class for the PresentateursPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-presentateurs',
  templateUrl: 'presentateurs.html',
})
export class PresentateursPage {

  public speakers: Speaker[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public dataProvider: DataProvider) {
    this.dataProvider.getSpeakers().then((speakers) => {
      this.speakers = speakers;
    });
  }

  ionViewDidLoad() { }
}
