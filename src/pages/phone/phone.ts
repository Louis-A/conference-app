import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { Network } from '../../../node_modules/@ionic-native/network';


/**
 * Generated class for the PhonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-phone',
  templateUrl: 'phone.html',
})
export class PhonePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public device: Device, public network: Network) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhonePage');
  }

}
