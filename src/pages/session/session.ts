import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Session } from '../../model/Session';
import { DataProvider } from '../../providers/data/data';
import { Speaker } from '../../model/Speaker';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';


/**
 * Generated class for the SessionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-session',
  templateUrl: 'session.html',
})
export class SessionPage {

  public session: Session;
  public speakers: Speaker[] = [];
  public note: string = "";
  public images: string[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public dataProvider: DataProvider, private storage: Storage, private camera: Camera, private imagePicker: ImagePicker, private file: File) {
    this.session = navParams.get('session');
    
    // Load speakers
    if(this.session.speakers) {
      for(let speakerId of this.session.speakers) {
        this.dataProvider.getSpeaker(speakerId).then((speaker) => {
          this.speakers.push(speaker);
        });
      }
    }

    // Load notes
    this.loadNote();
  }

  public saveNote() {
    this.storage.set('note_'+this.session.id, this.note);
  }

  public loadNote() {
    this.storage.get('note_'+this.session.id).then((note) => {
      if(note != null) {
        this.note = note;
      }
    });
  }

  public openCamera() {
    this.camera.getPicture({
      destinationType:  this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000
    }).then((image) => {
      this.images.push("data:image/jpeg;base64," + image);
    });
  }

  public getImageFromFilesystem() {
    let options: ImagePickerOptions = {
      maximumImagesCount: 1
    };

    this.imagePicker.getPictures(options).then((results) => {
        let file = results[0].substring(results[0].lastIndexOf('/') + 1);
        let path =  results[0].substring(0, results[0].lastIndexOf('/') + 1);
        this.file.readAsDataURL(path, file).then(res => {
          this.images.push(res);
        });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SessionPage');
  }
}
