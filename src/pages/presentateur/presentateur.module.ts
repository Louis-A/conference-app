import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PresentateurPage } from './presentateur';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PresentateurPage,
  ],
  imports: [
    IonicPageModule.forChild(PresentateurPage),
    ComponentsModule
  ],
})
export class PresentateurPageModule {}
