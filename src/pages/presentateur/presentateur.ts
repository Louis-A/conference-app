import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Speaker } from '../../model/Speaker';
import { Session } from '../../model/Session';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the PresentateurPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-presentateur',
  templateUrl: 'presentateur.html',
})
export class PresentateurPage {

  public speaker: Speaker;
  public sessions: Session[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public dataProvider: DataProvider) {
    this.speaker = navParams.get("speaker");

    this.dataProvider.getSessions().then((sessions) => {
      for (let session of sessions) {
        if(session.speakers) {   
          for(let speakerId of session.speakers) {
            if(speakerId == this.speaker.id) {
              this.sessions.push(session);
              break;
            }
          }
        }
      }
    });
  }

  ionViewDidLoad() { }

  
}
