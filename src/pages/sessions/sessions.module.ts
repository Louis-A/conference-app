import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SessionsPage } from './sessions';
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  declarations: [
    SessionsPage,
  ],
  imports: [
    IonicPageModule.forChild(SessionsPage),
    ComponentsModule
  ]
})
export class SessionsPageModule {}
