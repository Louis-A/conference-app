import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

@IonicPage()
@Component({
  selector: 'page-sessions',
  templateUrl: 'sessions.html',
})
export class SessionsPage {

  public timeslots: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public dataProvider: DataProvider) { }

  ionViewDidLoad() {
    this.dataProvider.getTimeSlots().then(timeslotsData => { 
      // We group the slots per date
      let timeslotsPerDate = timeslotsData.reduce((accumulator, timeslot) => {
        (accumulator[timeslot['dateReadable']] = accumulator[timeslot['dateReadable']] || []).push(timeslot);
        return accumulator;
      }, {});

      console.log(timeslotsPerDate);

      // We transform the object into an array
      let dates = Object.keys(timeslotsPerDate);
      for(let date of dates) {
        this.timeslots.push({
          "date": date,
          "timeslots": timeslotsPerDate[date]
        });
      }
    });
  }
}
