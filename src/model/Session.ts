import { Deserializable } from "./Deserializable";

export class Session implements Deserializable {
    public id: number;
    public title: string;
    public titleMobile: string;
    public image: string;
    public type: string;
    public description?: string;
    public track?: any; // track {title: "room"}
    public category?: string;
    public langage?: string;
    public tags?: any;
    public complexity?: string;
    public speakers?: number[];

    constructor() {}

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}