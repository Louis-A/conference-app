import { Deserializable } from "./Deserializable";

export class Speaker implements Deserializable {
    public id: number;
    public name: string;
    public featured: boolean;
    public company: string;
    public companyLogo: string;
    public country: string;
    public photoUrl: string;
    public shortBio: string;
    public bio: string;
    public tags: any;
    public badges: any;
    public socials: any;

    constructor() {}

    deserialize(input: any): this {
        Object.assign(this, input);
        
        if(this.photoUrl[0] != "/") {
            this.photoUrl = "/" + this.photoUrl;
        }

        return this;
    }
}