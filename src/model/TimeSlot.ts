import { Deserializable } from "./Deserializable";
import { Session } from "./Session";

export class TimeSlot implements Deserializable {
    
    public date: string;
    public dateReadable: string;
    public startTime: string;
    public endTime: string;
    public sessions: Session[];
    
    constructor () {}
    
    deserialize(input: any): this {
        Object.assign(this, input);
        
        this.sessions = [];
        for (let session of input["sessions"]) {
            this.sessions.push(new Session().deserialize(session));
        }

        return this;
    }
}