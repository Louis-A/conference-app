import { Component, Input } from '@angular/core';
import { Speaker } from '../../model/Speaker';
import { PresentateurPage } from '../../pages/presentateur/presentateur';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the PersonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'person',
  templateUrl: 'person.html'
})
export class PersonComponent {

  @Input() person: Speaker;

  constructor(private navCtrl: NavController) {}

  public displaySpeaker() {
    this.navCtrl.push(PresentateurPage, {
      "speaker": this.person
    });
  }
}
