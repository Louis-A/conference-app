import { Component, Input } from '@angular/core';
import { Session } from '../../model/Session';
import { SessionPage } from '../../pages/session/session';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the SessionSummaryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'session-summary',
  templateUrl: 'session-summary.html'
})
export class SessionSummaryComponent {

  @Input() session: Session;

  constructor(private navCtrl: NavController) {}

  public displayDetails() {
    this.navCtrl.push(SessionPage, {
      "session": this.session
    });
  }
}
