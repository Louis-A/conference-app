import { NgModule } from '@angular/core';
import { PersonComponent } from './person/person';
import { IonicModule } from 'ionic-angular';
import { SessionSummaryComponent } from './session-summary/session-summary';
@NgModule({
	declarations: [
		PersonComponent,
   		SessionSummaryComponent
	],
	imports: [IonicModule.forRoot(ComponentsModule)],
	exports: [
		PersonComponent,
		SessionSummaryComponent
	]
})
export class ComponentsModule {}
